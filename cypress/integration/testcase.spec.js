Cypress.on('uncaught:exception', (err, runnable) => {
    //returning false here prevents cypress from
    //failing the test
    return false
})

/// <reference types="Cypress" />
describe('Calculate the current exchange', function () {
    it('THB to USD', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);        
        cy.get('#selectSource').click();
        cy.get('#sourceTHB').click();
        cy.get('#selectTarget').click();
        cy.get('#targetUSD').click();
        cy.get('#calBtn').click();
        cy.get('#result').should('contain.text', '3.3333 USD');
    })

    it('check input number less than 0', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(-100);        
        cy.get('#selectSource').click();
        cy.get('#sourceTHB').click();
        cy.get('#selectTarget').click();
        cy.get('#targetUSD').click();
        cy.get('#calBtn').click();
        cy.get('#result').should('contain.text', 'Cannot calculate, please input number more than 0');
    })

    it('check letter input', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type('waan');        
        cy.get('#selectSource').click();
        cy.get('#sourceTHB').click();
        cy.get('#selectTarget').click();
        cy.get('#targetUSD').click();
        cy.get('#calBtn').click();
        cy.get('#result').should('contain.text', 'Cannot calculate, please input number');
    })

    it('EUR to THB', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(1000);        
        cy.get('#selectSource').click();
        cy.get('#sourceEUR').click();
        cy.get('#selectTarget').click();
        cy.get('#targetTHB').click();
        cy.get('#calBtn').click();
        cy.get('#result').should('contain.text', '33,333.3333 THB');
    })

    it('USD to EUR', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(500);        
        cy.get('#selectSource').click();
        cy.get('#sourceUSD').click();
        cy.get('#selectTarget').click();
        cy.get('#targetEUR').click();
        cy.get('#calBtn').click();
        cy.get('#result').should('contain.text', '450.00 EUR');
    })

})
