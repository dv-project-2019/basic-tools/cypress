Cypress.on('uncaught:exception', (err, runnable) => {
    //returning false here prevents cypress from
    //failing the test
    return false
})

/// <reference types="Cypress" />
describe('Calculate the current exchange', function () {
    it('THB to THB', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);        
        cy.get('#selectSource').click();
        cy.get('#sourceTHB').click();
        cy.get('#selectTarget').click();
        cy.get('#targetTHB').click();
        cy.get('#calBtn').click();
        cy.get('#result');
    })

    it('THB to USD', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);
        cy.get('#selectSource').click();
        cy.get('#sourceTHB').click();
        cy.get('#selectTarget').click();
        cy.get('#targetUSD').click();
        cy.get('#calBtn').click();
        cy.get('#result');
    })

    it('THB to EUR', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);
        cy.get('#selectSource').click();
        cy.get('#sourceTHB').click();
        cy.get('#selectTarget').click();
        cy.get('#targetEUR').click();
        cy.get('#calBtn').click();
        cy.get('#result');
    })

    it('USD to THB', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);
        cy.get('#selectSource').click();
        cy.get('#sourceUSD').click();
        cy.get('#selectTarget').click();
        cy.get('#targetTHB').click();
        cy.get('#calBtn').click();
        cy.get('#result');
    })

    it('USD to USD', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);
        cy.get('#selectSource').click();
        cy.get('#sourceUSD').click();
        cy.get('#selectTarget').click();
        cy.get('#targetUSD').click();
        cy.get('#calBtn').click();
        cy.get('#result');
    })

    it('USD to EUR', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);
        cy.get('#selectSource').click();
        cy.get('#sourceUSD').click();
        cy.get('#selectTarget').click();
        cy.get('#targetEUR').click();
        cy.get('#calBtn').click();
        cy.get('#result');
    })

    it('EUR to THB', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);
        cy.get('#selectSource').click();
        cy.get('#sourceEUR').click();
        cy.get('#selectTarget').click();
        cy.get('#targetTHB').click();
        cy.get('#calBtn').click();
        cy.get('#result');
    })

    it('EUR to USD', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);
        cy.get('#selectSource').click();
        cy.get('#sourceEUR').click();
        cy.get('#selectTarget').click();
        cy.get('#targetUSD').click();
        cy.get('#calBtn').click();
        cy.get('#result');
    })

    it('EUR to EUR', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency');
        cy.get('#sourceAmount').type(100);
        cy.get('#selectSource').click();
        cy.get('#sourceEUR').click();
        cy.get('#selectTarget').click();
        cy.get('#targetEUR').click();
        cy.get('#calBtn').click();
        cy.get('#result');
    })
})